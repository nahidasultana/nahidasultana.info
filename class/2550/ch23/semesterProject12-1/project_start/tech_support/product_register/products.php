<h1>Results</h1><br/>


<form action="index.php" method="post">
    <span>Customer:&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['firstName'] . ' ' . $_SESSION['lastName']; ?></span><br/><br/>
        <span>Product:&nbsp;&nbsp;&nbsp;
            <select name="product_type">
                <?php foreach (getProducts() as $product) : ?>
                    <option
                        value="<?php echo $product['productCode']; ?>"><?php echo $product['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </span>
    <input type="hidden" name="customerID" value="<?php echo $_SESSION['customerID']; ?>">
    <input type="submit" value="Register" name="register"><br/><br/>

    <h2><?php echo $statusMessage; ?></h2>
    <p>You are logged in as <?php echo $_SESSION['email']; ?></p><br/>
    <b id="logout"><a href="index.php?logout=1">Log Out</a></b><br/>
</form>
