<?php

function checkLoginEmail($email)
{
    $exist = FALSE;

    if (!empty($email)) {
        global $db;
        $email = strtolower($email);
        $sql = "SELECT customerID, firstName, lastName, email FROM customers WHERE LOWER(email) LIKE '" . $email . "'";
        $customers = $db->query($sql);

        if ($customers->rowCount() < 1) {
            $exist = FALSE;
        } else {
            $exist = TRUE;
            $customer = $customers->fetch();
            $_SESSION['email'] = $email;
            $_SESSION['firstName'] = $customer['firstName'];
            $_SESSION['lastName'] = $customer['lastName'];
            $_SESSION['customerID'] = $customer['customerID'];
        }
    }
    return $exist;
}

function getProducts()
{
    global $db;
    $sql = "SELECT productCode, name FROM `products` LIMIT 0, 30";
    $products = $db->query($sql);
    $productsArray = $products->fetchAll();
    return $productsArray;
}

function registerProduct($customerID, $productType) {
    $status = false;
    global $db;
    $registrationDate = date("Y-m-d H:i:s");
    $query = "INSERT INTO registrations(customerID, productCode, registrationDate) VALUES('$customerID', '$productType', '$registrationDate')";
    $insertedRow = $db->exec($query);
    if ($insertedRow > 0) {
        $status = true;
    }

    return $status;
}


