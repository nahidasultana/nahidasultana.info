<?php

require_once './database.php';
include './utils.php';

include_once './header.php';

session_start();

$statusMessage = '';
$isLogout = false;

if (isset($_GET['logout'])) {
    include './logout.php';
    $isLogout = true;
}

if (!$isLogout) {
    if (empty($_POST['login']) && empty($_SESSION['email'])) {
        include './login.php';
    } else {
        $isValidEmail = false;
        if (isset($_POST['login'])) {
            $email = $_POST['email'];
            $isValidEmail = checkLoginEmail($email);
            if (!$isValidEmail) {
                $statusMessage = '<h2 class = "not-update">Error: Invalid email address!</h2>';
                include './logout.php';
            }
        }

        if (isset($_POST['register'])) {
            $customerID = $_POST['customerID'];
            $productType = $_POST['product_type'];
            if (registerProduct($customerID, $productType)) {
                $statusMessage = '<h2 class = "update">Product '.$productType.' was registered successfully.</h2>';
            } else {
                $statusMessage = '<h2 class = "not-update">Product '.$productType.' has been already registered! Choose another one.</h2>';
            }
        }

        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            include './products.php';
        }
    }
}

include_once './footer.php';