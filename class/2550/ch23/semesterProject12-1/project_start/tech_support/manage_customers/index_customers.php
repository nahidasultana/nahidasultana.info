<?php
require_once('database.php');
?> 
<!DOCTYPE html>
<html>
    <!-- the head section -->
    <head>
        <title>SportsPro Technical Support</title>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <!-- the body section -->
    <body>
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="../index.php">Home</a></li>
                </ul>
            </nav>
        </header>
        <main>      
            <h1>Customer Search</h1>
            <form action="index_customers.php" method="post" id="search_customers">
                <label>Last Name:</label>
                <input type="text" name="searchString">
                <label>&nbsp;</label>
                <input type="submit" name="submit" value="Search"><br/>
            </form>

            <?php
            if (isset($_POST['submit'])) {
                $searchString = $_POST['searchString'];
                if (!empty($searchString)) {
                    $searchString = strtolower($searchString);

                    $sql = "SELECT customerID, firstName, lastName, email, city FROM customers WHERE LOWER(lastName) LIKE '%" . $searchString . "%'";
                    $customers = $db->query($sql);
                   
                    if ($customers->rowCount() < 1) {
                        echo '<h2 class = "no-found">No results found.</h2>';
                    } else {
                        ?>
                        <h1>Results</h1>
                        <table>
                            <tr>
                                <th colsan="2">Name</th>
                                <th>Email Address</th>
                                <th>City</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php foreach ($customers as $customer) : ?> 
                                <tr>
                                    <td><?php echo $customer['firstName'] . ' ' . $customer['lastName']; ?></td>
                                    <td><?php echo $customer['email']; ?></td>
                                    <td><?php echo $customer['city']; ?></td>
                                    <td>
                                        <form action="customer.php" method="post">
                                            <input type="hidden" name="customerID" value="<?php echo $customer['customerID']; ?>">
                                            <input type="submit" value="Select">
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>

                        <?php
                    }
                } else {
                    echo "No result found.";
                }
            }
            ?>
        </main>
        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body>
</html>