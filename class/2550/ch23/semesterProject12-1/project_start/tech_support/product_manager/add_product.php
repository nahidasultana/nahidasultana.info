<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <!-- the head section -->
    <head>
        <title>SportsPro Technical Support</title>
        <link rel="stylesheet" type="text/css" href="main.css" />
    </head>
    <body>
        <?php
// Get the product data
        $product_code = $_POST['productCode'];
        $product_name = $_POST['name'];
        $product_version = $_POST['version'];
        $product_release_date = $_POST['releaseDate'];

// Validate inputs
        if (empty($product_code) || empty($product_name) || empty($product_version) || empty($product_release_date)) {
            $error = "Invalid product data. Check all fields and try again.";
            include('error.php');
        } else {
            // If valid, add the product to the database
            require_once('database.php');
            $query = "INSERT INTO products
                 (productCode, name, version, releaseDate)
              VALUES('$product_code', '$product_name', '$product_version', '$product_release_date' )";
            $db->exec($query);

            // Display the Product List page
            include('index-product.php');
        }
        ?>
    </body>
</html>
