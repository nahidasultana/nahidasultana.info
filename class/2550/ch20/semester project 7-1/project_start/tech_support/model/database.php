<?php
    $dsn = 'mysql:host=p3plcpnl0793.prod.phx3.secureserver.net;dbname=nahida_tech_support';
    $username = 'nahida_ts_user';
    $password = 'pa55word';

    try {
        $db = new PDO($dsn, $username, $password);
    } catch (PDOException $e) {
        $error_message = $e->getMessage();
        include('../errors/database_error.php');
        exit();
    }
?>