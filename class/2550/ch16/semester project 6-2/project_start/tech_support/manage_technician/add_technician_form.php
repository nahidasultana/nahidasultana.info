<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <!-- the head section -->
    <head>
        <title>SportsPro Technical Support</title>
        <link rel="stylesheet" type="text/css" href="main.css" />
    </head>
    <body>
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="../index.php">Home</a></li>
                </ul>
            </nav>
        </header><main id="aligned">
            <h1>Add Technician</h1>
            <form action="add_technician.php" method="post" id="add_technician_form">
                <label>First Name:</label><input type="text" name="firstName"/><br/>
                <label>Last Name:</label><input type="text" name="lastName"/><br/>
                <label>Email:</label><input type="text" name="email"/><br/>
                <label>Phone:</label><input type="text" name="phone"/><br>
                    <label>Password:</label><input type="text" name="password"/><br></br>
                    <label>&nbsp;</label><input type="submit" value="Add Technician"/><br/>
            </form>
            <p><a href="index_tech.php">View Technician List</a></p>
        </main>

        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body>
</html>