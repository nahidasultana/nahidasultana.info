<?php
require_once('database.php');

// Get products for selected category 
$query = "SELECT * FROM `technicians` LIMIT 0, 30 ";
$technicians = $db->query($query);
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
    <!-- the head section --> 
    <head> 
        <title>SportsPro Technical Support</title> 
        <link rel="stylesheet" type="text/css" href="main.css" /> 
    </head> 

    <!-- the body section --> 
    <body> 
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="../index.php">Home</a></li>
                </ul>
            </nav>
        </header>

        <main>
            <!-- display a table of products --> 
            <h2>Technician List</h2> 
            <table> 
                <tr> 
                    <th>Tech ID</th> 
                    <th>Firs Name</th> 
                    <th>Last Name</th> 
                    <th>Email</th> 
                    <th>Phone</th>
                    <th>Password</th>
                    <th>&nbsp;</th> 
                </tr> 
                <?php foreach ($technicians as $technician) : ?> 
                    <tr> 
                        <td><?php echo $technician['techID']; ?></td> 
                        <td><?php echo $technician['firstName']; ?></td> 
                        <td><?php echo $technician['lastName']; ?></td> 
                        <td><?php echo $technician['email']; ?></td> 
                        <td><?php echo $technician['phone']; ?></td>
                        <td><?php echo $technician['password']; ?></td> 
                        <td>
                            <form action="delete_technician.php" method="post" id = "delete_product_form"> 
                                <input type="hidden" name="tech_id" value="<?php echo $technician['techID']; ?>" /> 
                                <input type="submit" value="Delete" /> 
                            </form>
                        </td> 
                    </tr> 
                <?php endforeach; ?> 
            </table> 
            <p><a href="add_technician_form.php">Add Technician</a></p> 
            </div> 
        </main>

        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body> 
</html>