<?php
// Constant for OpenShift
$dbUrl = getenv('OPENSHIFT_MYSQL_DB_HOST');
$dbPort = getenv('OPENSHIFT_MYSQL_DB_PORT');

//Variable for my projects.
$dbName = 'my_guitar_shop2';
$username = 'mgs_user';
$password = 'pa55word';

$dsn = 'mysql:host=' . $dbUrl . ';port=' . $dbPort . ';dbname=' . $dbName;
$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

try {
    $db = new PDO($dsn, $username, $password, $options);
} catch (PDOException $e) {
    $error_message = $e->getMessage();
    include 'errors/db_error_connect.php';
    exit;
}

function display_db_error($error_message) {
    global $app_path;
    include 'errors/db_error.php';
    exit;
}
?>