<?php
// Get the product data
$technician_first_name = $_POST['firstName'];
$technician_last_name = $_POST['lastName'];
$technician_email = $_POST['email'];
$technician_phone = $_POST['phone'];
$technician_password = $_POST['password'];

// Validate inputs
if (empty($technician_first_name) || empty($technician_last_name) || empty($technician_email) || empty($technician_phone) || empty($technician_password) ) {
    $error = "Invalid product data. Check all fields and try again.";
    include('error.php');
} else {
    // If valid, add the product to the database
    require_once('database.php');
    $query = "INSERT INTO technicians
                 (firstName, lastName, email, phone, password)
              VALUES('$technician_first_name', '$technician_last_name', '$technician_email', '$technician_phone', '$technician_password')";
    $db->exec($query);

    // Display the Product List page
    include('index_tech.php');
}
?>