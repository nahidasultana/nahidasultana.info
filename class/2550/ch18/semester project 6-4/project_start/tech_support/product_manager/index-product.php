<?php
require_once('database.php');

// Get products for selected category 
$query = "SELECT * FROM `products` LIMIT 0, 30 ";
$products = $db->query($query);
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
    <!-- the head section --> 
    <head> 
        <title>SportsPro Technical Support</title> 
        <link rel="stylesheet" type="text/css" 
              href="main.css" /> 
    </head> 

    <!-- the body section --> 
    <body> 
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="http://www.nahidasultana.info/class/2550/ch16/semester%20project%206-2/project_start/tech_support/index.php">Home</a></li>
                </ul>
            </nav>
        </header>

        <main>
            <!-- display a table of products --> 
            <h2>Product List</h2> 
            <table> 
                <tr> 
                    <th>Code</th> 
                    <th>Name</th> 
                    <th>Version</th> 
                    <th>Release Date</th> 
                    <th>&nbsp;</th> 
                </tr> 
                <?php foreach ($products as $product) : ?> 
                    <tr> 
                        <td><?php echo $product['productCode']; ?></td> 
                        <td><?php echo $product['name']; ?></td> 
                        <td><?php echo $product['version']; ?></td> 
                        <td><?php echo $product['releaseDate']; ?></td> 
                        <td>
                            <form action="delete_product.php" method="post" id = "delete_product_form"> 
                                <input type="hidden" name="product_code" value="<?php echo $product['productCode']; ?>" /> 
                                <input type="submit" value="Delete" /> 
                            </form>
                        </td> 
                    </tr> 
                <?php endforeach; ?> 
            </table> 
            <p><a href="add_product_form.php">Add Product</a></p> 
            </div> 
        </main>

        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body> 
</html>