<?php
require_once('database.php');
$sql = "SELECT productCode, name FROM `products` LIMIT 0, 30 ";
$products = $db->query($sql);
$productsArray = $products->fetchAll();
?> 
<!DOCTYPE html>
<html>
    <!-- the head section -->
    <head>
        <title>SportsPro Technical Support</title>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <!-- the body section -->
    <body>
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="../index.php">Home</a></li>
                </ul>
            </nav>
        </header>
        <main> 

            <?php
            $error = FALSE;
            $message = "";

            if (isset($_POST['register'])) {
                $productType = $_POST['product_type'];
                $customerID = $_POST['customerID'];
                $registrationDate = date("Y-m-d H:i:s");
                $query = "INSERT INTO registrations(customerID, productCode, registrationDate) VALUES('$customerID', '$productType', '$registrationDate')";
                $insertedRow = $db->exec($query);
                if ($insertedRow > 0) {
                    $error = FALSE;
                    $message = '<h2 class = "update">Product '.$productType.' was registered successfully.</h2>';
                } else {
                    $error = TRUE;
                    $message = '<h2 class = "not-update">Product '.$productType.' has been already registered! Choose another one.</h2>';
                }
            }


            if (isset($_POST['submit'])) {
                $searchString = $_POST['searchString'];
                if (!empty($searchString)) {
                    $searchString = strtolower($searchString);
                    $sql = "SELECT customerID, firstName, lastName, email FROM customers WHERE LOWER(email) LIKE '" . $searchString . "'";
                    $customers = $db->query($sql);

                    if ($customers->rowCount() < 1) {
                        $error = TRUE;
                        $message = '<h2 class = "no-found">No results found.</h2>';
                    } else {
                        ?>
                        <h1>Results</h1><br/>
                        <?php foreach ($customers as $customer) : ?> 
                            <form action="login_customer.php" method="post">
                                <span>Customer:&nbsp;&nbsp;&nbsp;<?php echo $customer['firstName'] . ' ' . $customer['lastName']; ?></span><br/><br/>
                                <span>Product:&nbsp;&nbsp;&nbsp;
                                    <select name="product_type">
                                        <?php foreach ($productsArray as $product) : ?> 
                                            <option value="<?php echo $product['productCode']; ?>"><?php echo $product['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </span>
                                <input type="hidden" name="customerID" value="<?php echo $customer['customerID']; ?>">
                                <input type="submit" value="Register" name="register">
                            </form>

                        <?php endforeach; ?>
                        <?php
                    }
                } else {
                    $error = TRUE;
                    $message = '<h2 class = "not-update">Please enter your valid email address.';
                }
            }
            ?>

            <?php
            if (!isset($_POST['submit']) || $error) {
                ?>
                <h1>Customer Login</h1><br/>
                <p>You must login before you can register a product.</p><br/>
                <form action="login_customer.php" method="post" id="search_customers">
                    <label>Email:</label>
                    <input type="text" name="searchString">
                    <label>&nbsp;</label>
                    <input type="submit" name="submit" value="Login"><br/>
                </form>
                <?php
                echo $message;
            }
            ?>
        </main>
        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body>
</html>