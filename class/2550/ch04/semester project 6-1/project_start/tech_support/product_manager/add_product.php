<?php
// Get the product data
$product_code = $_POST['productCode'];
$product_name = $_POST['name'];
$product_version = $_POST['version'];
$product_release_date = $_POST['releaseDate'];

// Validate inputs
if (empty($product_code) || empty($product_name) || empty($product_version) || empty($product_release_date) ) {
    $error = "Invalid product data. Check all fields and try again.";
    include('error.php');
} else {
    // If valid, add the product to the database
    require_once('database.php');
    $query = "INSERT INTO products
                 (productCode, name, version, releaseDate)
              VALUES('$product_code', '$product_name', '$product_version', '$product_release_date' )";
    $db->exec($query);

    // Display the Product List page
    include('index-product.php');
}
?>