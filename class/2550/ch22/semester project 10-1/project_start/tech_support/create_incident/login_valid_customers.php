<?php
require_once('database.php');
?> 
<!DOCTYPE html>
<html>
    <!-- the head section -->
    <head>
        <title>SportsPro Technical Support</title>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <!-- the body section -->
    <body>
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="../index.php">Home</a></li>
                </ul>
            </nav>
        </header>
        <main id="aligned"> 

            <?php
            $error = FALSE;
            $message = "";

            if (isset($_POST['incident'])) {               
                if (!empty($_POST[' title'])||!empty($_POST['description'])) {
                    $productType = $_POST['product_type'];
                    $customerID = $_POST['customerID'];
                    $dateOpened = date("Y-m-d H:i:s");
                    $dateClosed = date("Y-m-d H:i:s");
                    $title = $_POST['title'];
                    $description = $_POST['description'];
                    $query = "INSERT INTO incidents(customerID, productCode, dateOpened, dateClosed, title, description) VALUES('$customerID', '$productType', '$dateOpened', '$dateClosed', '$title', '$description')";
                    $insertedRow = $db->exec($query);
                    if ($insertedRow > 0) {
                        $error = FALSE;
                        $message = '<h2 class = "update">This incident was added successfully to our database.</h2>';
                    } else {
                        $error = TRUE;
                        $message = '<h2 class = "not-update">ERROR!!!</h2>';
                    }
                } else {
                    $error = TRUE;
                    $message = '<h2 class = "not-update">All fields must be filled. Please try again!</h2>';
                }
            }


            if (isset($_POST['submit'])) {
                $searchString = $_POST['searchString'];
                if (!empty($searchString)) {
                    $searchString = strtolower($searchString);
                    $sql = "SELECT customerID, firstName, lastName, email FROM customers WHERE LOWER(email) LIKE '" . $searchString . "'";
                    $customers = $db->query($sql);

                    if ($customers->rowCount() < 1) {
                        $error = TRUE;
                        $message = '<h2 class = "no-found">No results found.</h2>';
                    } else {
                        ?>
                        <h1>Create Incident</h1><br/>
            <?php foreach ($customers as $customer) : ?> 
                            <form action="login_valid_customers.php" method="post">
                                <label>Customer:</label><?php echo $customer['firstName'] . ' ' . $customer['lastName']; ?><br/><br/>
                                <label>Product:</label>
                                <select name="product_type">
                                    <?php
                                    $sql = "SELECT * FROM products WHERE productCode IN (SELECT productCode FROM registrations WHERE customerID = " . $customer['customerID'] . ")";
                                    $products = $db->query($sql);
                                    $productsArray = $products->fetchAll();
                                    foreach ($productsArray as $product) :
                                        ?> 
                                        <option value="<?php echo $product['productCode']; ?>"><?php echo $product['name']; ?></option>
                <?php endforeach; ?>
                                </select><br/><br/>
                                <label>Title:</label>
                                <input type="text" name="title"><br><br/><br/>
                                <label>Description:</label>
                                <textarea rows="4" cols="50" name="description"></textarea><br/><br/>
                                <input type="hidden" name="customerID" value="<?php echo $customer['customerID']; ?>">
                                <input type="submit" class="create-incident-button" value="Create Incident" name="incident">
                            </form>

                        <?php endforeach; ?>
                        <?php
                    }
                } else {
                    $error = TRUE;
                    $message = '<h2 class = "not-update">Please enter your valid email address.';
                }
            }
            ?>

            <?php
            if (!isset($_POST['submit']) || $error) {
                ?>
                <h1>Get Customer</h1><br/>
                <p>You must login before you can register a product.</p><br/>
                <form action="login_valid_customers.php" method="post" id="search_customers">
                    <label>Email:</label>
                    <input type="text" name="searchString">
                    <label>&nbsp;</label>
                    <input type="submit" class = "get-customer" name="submit" value="Get Customer"><br/>
                </form>
                <?php
                echo $message;
            }
            ?>
        </main>
        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body>
</html>