<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <!-- the head section -->
    <head>
        <title>SportsPro Technical Support</title>
        <link rel="stylesheet" type="text/css" href="main.css" />
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            $(function () {
                $(".datepicker").datepicker();
            });
        </script>

    </head>
    <body>
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="../index.php">Home</a></li>
                </ul>
            </nav>
        </header><main id="aligned">
            <h1>Add Product</h1>
            <form action="add_product.php" method="post" id="add_product_form">
                <label>Code:</label><input type="text" name="productCode"/><br/>
                <label>Name:</label><input type="text" name="name"/><br/>
                <label>Version:</label><input type="text" name="version"/><br/>
                <label>Release Date:</label><input type="text" name="releaseDate" class = "datepicker"/>&nbsp;Use 'yyyy-mm-dd' format<br/>
                <label>&nbsp;</label><input type="submit" value="Add Product"/><br>
            </form>
            <p><a href="index-product.php">View Product List</a></p>
        </main>

        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body>
</html>