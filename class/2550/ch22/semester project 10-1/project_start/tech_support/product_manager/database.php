<?php
// Constant for OpenShift
$dbUrl = getenv('OPENSHIFT_MYSQL_DB_HOST');
$dbPort = getenv('OPENSHIFT_MYSQL_DB_PORT');

//Variable for my projects.
$dbName = 'nahida_tech_support';
$username = 'root';
$password = 'sesame';

$dsn = 'mysql:host=' . $dbUrl . ';port=' . $dbPort . ';dbname=' . $dbName;

try {
    $db = new PDO($dsn, $username, $password);
} catch (PDOException $e) {
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}
?>