<?php
require_once('database.php');
?>
<!DOCTYPE html>
<html>
    <!-- the head section -->
    <head>
        <title>SportsPro Technical Support</title>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>

    <!-- the body section -->
    <body>
        <header>
            <h1>SportsPro Technical Support</h1>
            <p>Sports management software for the sports enthusiast</p>
            <nav>
                <ul>
                    <li><a href="../index.php">Home</a></li>
                </ul>
            </nav>
        </header>
        <main id="aligned">      
            <h1>View/Update Customer</h1>
            <?php
            if (isset($_POST['update_customer'])) {
                $update_sql = "UPDATE customers SET "
                        . "customerID='" . $_POST['customerID'] . "'"
                        . ",firstName='" . $_POST['firstName'] . "'"
                        . ",lastName='" . $_POST['lastName'] . "'"
                        . ",address='" . $_POST['address'] . "'"
                        . ",city='" . $_POST['city'] . "'"
                        . ",state='" . $_POST['state'] . "'"
                        . ",postalCode='" . $_POST['postalCode'] . "'"
                        . ",countryCode='" . $_POST['countryCode'] . "'"
                        . ",phone='" . $_POST['phone'] . "'"
                        . ",email='" . $_POST['email'] . "'"
                        . ",password='" . $_POST['password'] . "'"
                        . " WHERE customerID='" . $_POST['customerID'] . "'";
                $updatedRow = $db->exec($update_sql);
                if ($updatedRow > 0) {
                    echo '<h2 class = "update">Updated Successfully!</h2>';
                } else {
                    echo '<h2 class = "not-update">Not Updated!</h2>';
                }
            }
            $customer_id = isset($_POST['customerID']) ? $_POST['customerID'] : 0;
            $select_sql = "SELECT * FROM customers WHERE customerID = '$customer_id'";
            $customers = $db->query($select_sql);
            $customer = $customers->fetch();
            ?>
            <!-- display View/Update Customer Form -->
            <form action="customer.php" method="post">
                <input type="hidden" name="customerID" value="<?php echo $customer['customerID']; ?>">
                <label>First Name:</label>
                <input type="text" name="firstName" value="<?php echo $customer['firstName']; ?>"><br>
                <label>Last Name:</label>
                <input type="text" name="lastName" value="<?php echo $customer['lastName']; ?>"><br>
                <label>Address:</label>
                <input type="text" name="address" value="<?php echo $customer['address']; ?>"><br>
                <label>City:</label>
                <input type="text" name="city" value="<?php echo $customer['city']; ?>"><br>
                <label>State:</label>
                <input type="text" name="state" value="<?php echo $customer['state']; ?>"><br>
                <label>Postal Code:</label>
                <input type="text" name="postalCode" value="<?php echo $customer['postalCode']; ?>"><br>
                <label>Country Name:</label>
                <select name="countryCode">
                    <?php
                    $sql = "SELECT * FROM countries";
                    $countries = $db->query($sql);
                    $countriesArray = $countries->fetchAll();
                    foreach ($countriesArray as $country) :
                        ?> 
                        <option value="<?php echo $country['countryCode']; ?>" <?php if ($country['countryCode'] == $customer['countryCode']) { ?>selected="selected"<?php } ?>>
                            <?php echo htmlspecialchars($country['countryName']); ?>
                        </option>
                    <?php endforeach; ?>
                </select><br/>
                <label>Phone:</label>
                <input type="text" name="phone" value="<?php echo $customer['phone']; ?>"><br>
                <label>Email:</label>
                <input type="email" name="email" value="<?php echo $customer['email']; ?>"><br>
                <label>Password:</label>
                <input type="text" name="password" value="<?php echo $customer['password']; ?>"><br>
                <label>&nbsp;</label>
                <input type="submit" value="Update Customer" name="update_customer"><br>
            </form>
            <p class="last_paragraph"><a href="index_customers.php">Search Customers</a></p>
        </main>
        <footer>
            <p class="copyright">
                &copy; 2015 SportsPro, Inc.
            </p>
        </footer>
    </body>
</html>
