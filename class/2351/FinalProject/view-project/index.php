<?php
include('login.php'); // Includes Login Script

if (isset($_SESSION['login_user'])) {
    header("location: profile.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login Form in PHP with Session</title>
        <link href="style-1.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="main">
                <div class = "header">
                    <h2>Login Form</h2>
                </div>
                <form action="" method="post">
                    <label>UserName* :</label>
                    <input id="name" name="username" placeholder="username" type="text"><br/>
                    <label>Password* :</label>
                    <input id="password" name="password" placeholder="**********" type="password"><br/>
                    <input type = "reset" class = "button" value = "Clear Form" />&nbsp;&nbsp;<input name="submit" type="submit" value="Login" class = "button"><br/><br/>
                    <span><?php echo $error; ?></span>
                </form>
                <div id="note"><span><b>Note : </b></span> Please Use Following Username And Password for Login. <br><br>
                    <b>username: </b>userone&nbsp;&nbsp;&nbsp;<b>password: </b>pwo<br>
                    <b>username: </b>usertwo&nbsp;&nbsp;&nbsp;<b>password: </b>pwt<br>
                </div>
            </div>
        </div>
    </body>
</html>