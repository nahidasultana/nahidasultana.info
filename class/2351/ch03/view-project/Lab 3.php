<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Concatenates Three Strings</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <style type="text/css">
            .wrapper{
                margin: 0 auto;
                width:32%;
                background-color: #F9F8F8;
                border: 1px solid #000000;
                padding:5px 0px 20px 10px;
                font-size: 100%;
                font-family: georgia,"times new roman",times,serif;
                line-height: 1.4;
            }
            input{
                margin-bottom: 10px;
            }

        </style>
    </head>
    <body>
        <div class = "wrapper">
		<p>Ch03 Lab: Write a PHP script that concatenates three strings - "Now is the time", "for all good citizens", and "to come to the aid of their country." Compare the cancatenated string with the actual quote (look it up).</p><hr/>
		<p><strong>Here is my PHP script:</strong></p>
            <?php
            $string1 = "Now is the time";
            $string2 = "for all good citizens";
            $string3 = "to come to the aid of their country.";
            
            $final = $string1 . $string2 . $string3;
            
            echo " <b>Concatenated  string:</b> " . $final;
            echo '<br/>';
            $quoteStr = "Now is the time for all good citizens to come to the aid of their country.";
            echo "<b>Quoted string:</b> " . $quoteStr;
            echo '<br/>';
            
            if($final == $quoteStr) {
                echo 'Comparison Result: Matched';
            } else {
                echo '<b>Comparison Result:</b> Not matched';
                echo "<p>Both strings have " . similar_text($final, $quoteStr) . " character(s) in common.</p>";
                echo "<p>You must change " . levenshtein($final, $quoteStr) . " character(s) to make the strings same.</p><br/>";
            }
            ?>
        </div>
    </body>
</html>