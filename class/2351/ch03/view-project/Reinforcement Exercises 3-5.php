<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Password Strength</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    </head>
    <body>
        <h1>Password Strength</h1><hr/>
        <?php
        $passwordArray = array("NoDigitsOrOthers", "NoDigits...", "NoOthers999", "2 Spaces !", "0LOWERCASE?", "0uppercase+", "Sh0r+", "This1IsMuchTooLongForAPassword!", "1GoodPassword", "GoodPassword2!");
        foreach ($passwordArray as $password) {
            $isValid = TRUE;
            echo "Checking \"$password\"<br/><hr/>";

            if (!preg_match("/[A-Z]/", $password)) {
                echo "Warning: \"$password\" has no uppercase letters.<br/>";
                $isValid = FALSE;
            }
            if (!preg_match("/[a-z]/", $password)) {
                echo "Warning: \"$password\" has no lowercase letters.<br/>";
                $isValid = FALSE;
            }
            if (!preg_match("/[0-9]/", $password)) {
                echo "Warning: \"$password\" has no numbers.<br/>";
                $isValid = FALSE;
            }
            if (!preg_match("/\W+/", $password)) {
                echo "Warning: \"$password\" has no non-alphanumeric characters.<br/>";
                $isValid = FALSE;
            }
            if (preg_match("/\s+/", $password)) {
                echo "Warning: \"$password\" contains one or more spaces.<br/>";
                $isValid = FALSE;
            }
            if (strlen("$password") < 8) {
                echo "Warning: \"$password\" has less than 8 characters. <br/>";
                $isValid = FALSE;
            }
            if (strlen("$password") > 16) {
                echo "Warning: \"$password\" has more than 16 characters. <br/>";
                $isValid = FALSE;
            }

            if ($isValid) {
                echo "\"$password\"is a strong password.<br/><hr/>";
            } else {
                echo "\"$password\"is not a strong password.<br/><hr/>";
            }
        }
        ?>
    </body>
</html>
