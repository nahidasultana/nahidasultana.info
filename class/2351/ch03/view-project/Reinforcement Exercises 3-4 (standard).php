<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Standard Palindrome</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    </head>
    <body>
        <h1>Standard Palindrome</h1><hr/>
        <?php
        
        $standardPalindrome = "Madam, I'm Adam";
        $standardPalindromeReplaced = strtolower(preg_replace("([^A-Za-z0-9])", "", $standardPalindrome));
        $standardPalindromeReplacedReversed = strrev($standardPalindromeReplaced);
        if ($standardPalindromeReplaced == $standardPalindromeReplacedReversed) {
            echo "<strong>\"$standardPalindrome\"</strong> is a Standard Palindrome.<br/><br/>";
        } else {
            echo "<strong>\"$standardpalindrome\"</strong> is not a Standard Palindrome.";
        }
        
        $perfectPalindrome = "racecars";
        $perfectPalindromeReplaced = strtolower($perfectPalindrome);
        $perfectPalindromeReplacedReversed = strrev($perfectPalindromeReplaced);
        if ($perfectPalindromeReplaced == $perfectPalindromeReplacedReversed) {
            echo "<strong>\"$perfectPalindrome\"</strong> is a Standard Palindrome.<br/><br/>";
        } else {
            echo "<strong>\"$perfectPalindrome\"</strong> is not a Standard Palindrome.";
        }
        
        ?>
    </body>
</html>