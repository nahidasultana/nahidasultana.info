<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Sign Tournament Book</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="main">
                <?php

                function displayError($fieldName, $errorMsg) {
                    global $errorCount;
                    echo "Error: \"$fieldName\" $errorMsg<br/>\n";
                    ++$errorCount;
                }

                function validateName($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        displayError($fieldName, "<span style='color:red;'>This field is required.</span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn'y empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        if ((is_numeric($retval))) {
                            displayError($fieldName, "<span style='color:red;'>must be a string value.</span>");
                        }
                    }
                    return($retval);
                }

                function validateAge($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        displayError($fieldName, "<span style='color:red;'>This field is required.</span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn'y empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        if (!(is_numeric($retval))) {
                            displayError($fieldName, "<span style='color:red;'>must be a numeric value.</span>");
                        }
                    }
                    return($retval);
                }

                function validateAverage($data, $fieldName) {
                    global $errorCount;
                    if (empty($data) && $data != "0") {
                        displayError($fieldName, "<span style='color:red;'>This field is required (Enter 0 if you don't have one).</span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn'y empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        $pattern = "/^[0-9]+$/";
                        if (!(is_numeric($retval))) {
                            displayError($fieldName, "<span style='color:red;'>must be a numeric value.</span>");
                        }
                    }
                    return($retval);
                }

                $errorCount = 0;
                $Name = validateName($_POST['name'], "Name");
                $Age = validateAge($_POST['age'], "Age");
                $Average = validateAverage($_POST['average'], "Average");
                if ($errorCount > 0)
                    echo "Please use the \"Back\" button to re-enter the data.<br/>\n";
                else {
                    $TournamentBook = fopen("tournamentbook.txt", "ab");
                    if (is_writable("tournamentbook.txt")) {
                        if (fwrite($TournamentBook, "Name: $Name" . ", " . "Age: $Age" . ", " . "Average: $Average" . "\n")) {
                            echo "<p>Thank you for signing our tournament book!</p>\n";
                        } else {
                            echo "<p> Cannot add your name to the tournament book.</p>\n";
                        }
                    } else {
                        echo "<p>Cannot write to the file.</p>\n";
                        fclose($TournamentBook);
                    }
                }
                ?>
            </div>
        </div>
    </body>
</html>

