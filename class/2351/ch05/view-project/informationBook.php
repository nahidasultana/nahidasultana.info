<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Information</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="main">
                <?php

                function displayError($fieldName, $errorMsg) {
                    global $errorCount;
                    echo "Error: \"$fieldName\" $errorMsg<br/>\n";
                    ++$errorCount;
                }

                function validateName($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        displayError($fieldName, "<span style='color:red;'>This field is required.</span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn'y empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        if ((strlen($retval) < 4)){
                            displayError($fieldName, "<span style='color:red;'>User name must be at least four characters long.</span>");
                        }
                        if ((is_numeric($retval))) {
                            displayError($fieldName, "<span style='color:red;'>must be a string value.</span>");
                        }
                    }
                    return($retval);
                }

                function validateEmail($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        displayError($fieldName, "<span style='color:red;'>This field is required.</span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn't empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        $pattern = "/^[_a-z0-9-+]+(\.[_a-z0-9-+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
                        if (preg_match($pattern, $retval) == 0) {
                            displayError($fieldName, "<span style='color:red;'>is not a valid e-mail address.</span><br/>\n");
                        }
                    }
                    return ($retval);
                }

                $errorCount = 0;
                $UserName = validateName($_POST['sender'], "User Name");
                $Email = validateEmail($_POST['email'], "Email Address");
                if ($errorCount > 0)
                    echo "Please use the \"Back\" button to re-enter the data.<br/>\n";
                else {
                    $InformationBook = fopen("informationbook.txt", "ab");
                    if (is_writable("informationbook.txt")) {
                        if (fwrite($InformationBook, "User Name: $UserName" . ", " . "Email Address: $Email" . "\n")) {
                            echo "<p>Thank you for subscribing, $UserName.</p>\n";
                        } else {
                            echo "<p> Cannot add your name here.</p>\n";
                        }
                    } else {
                        echo "<p>Cannot write to the file.</p>\n";
                        fclose($InformationBook);
                    }
                }
                ?>
            </div>
        </div>
    </body>
</html>

