<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Box Array</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="main">
                    <?php
                    $box = array(array("Small Box", "12\"L", "10\"W", "2.5\"D"),
                        array("Medium Box", "30\"L", "20\"W", "4\"D"),
                        array("Large Box", "60\"L", "40\"W", "11.5\"D")
                    );

                    foreach ($box as $value) {
                        echo $value[0] . " = " . $value[1] . " x " . $value[2] . " x " . $value[3] . " = " . $value[1] * $value[2] * $value[3] . " cu." . " in." . "<br />";
                    }
                    ?>		
            </div>
        </div>
    </body>
</html>
