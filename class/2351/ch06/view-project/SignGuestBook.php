<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Sign Guest Book</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="main">
                <?php
				function displayError($fieldName, $errorMsg) {
                    global $errorCount;
                    echo "Error: \"$fieldName\" $errorMsg<br/>\n";
                    ++$errorCount;
                }

                function validateName($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        displayError($fieldName, "<span style='color:red;'>This field is required.</span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn'y empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
						$pattern = "/^[a-zA-Z'. -]+$/";
                        if ((preg_match($pattern, $retval) == 0)||(is_numeric($retval))) {
                            displayError($fieldName, "<span style='color:red;'>must be a string value.</span>");
                        }
                    }
                    return($retval);
                }
				function validateEmail($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        displayError($fieldName, "<span style='color:red;'>This field is required.</span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn't empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        $pattern = "/^[_a-z0-9-+]+(\.[_a-z0-9-+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
                        if (preg_match($pattern, $retval) == 0) {
                            displayError($fieldName, "<span style='color:red;'>is not a valid e-mail address.</span><br/>\n");
                        }
                    }
                    return ($retval);
                }
				$errorCount = 0;
                $FirstName = validateName($_POST['first_name'], "First Name");
                $LastName = validateName($_POST['last_name'], "Last Name");
				$Email = validateEmail($_POST['email'], "Email Address");
                if ($errorCount > 0)
                    echo "Please use the \"Back\" button to re-enter the data.<br/>\n";
                else {
                    $GuestBook = fopen("guestbook.txt", "ab");
                     if (is_writable("guestbook.txt")){
                        if (fwrite($GuestBook, "First Name: $FirstName" . ", " . "Last Name: $LastName" . " , " . "Email Address: $Email" . "\n")) {
                            echo "<p>Thank you for signing our guest book!</p>\n";
                        } else {
                            echo "<p> Cannot add your name to the guest book.</p>\n";
                        }
                    } else {
                        echo "<p>Cannot write to the file.</p>\n";
                        fclose($GuestBook);
                    }
                }
                ?>
            </div>
        </div>
    </body>
</html>
