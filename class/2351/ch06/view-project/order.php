<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>European Travel</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="style-1.css" />
    </head>
    <body>
        <?php
        $priceList = array(100, 220, 440, 550, 310);
        ?>
        <div class="container">
            <form action="order.php" method="post">
                <div class="main">
                    <h1>Online Shopping Order</h1>
                    <div class ="products">
                        <div class = "header">
                            <h2>Product Name</h2>
                            <h2>Product Description</h2>
                            <h2>Product Quantity</h2>
                            <h2>Unit Price</h2>
                            <h2>Total Price</h2>
                        </div>
                        <div class="productsList">
                            <!--<img src="product-images/camera.jpg"/><br/>-->
                            <span class="top">HINGED BANGLE</span>
                            <span>Strong lines meet striking numerals in a contemporary design that honors a Tiffany icon. Hinged bangle in 18k rose gold with round brilliant diamonds. Medium, fits wrists up to 6.25" in circumference. </span>
                            <span class="top">
                                <input type="number" min="0" name="quantity[]" value="<?php echo isset($_POST['quantity']) ? $_POST['quantity'][0] : "0"; ?>" />
                            </span>
                            <span class="top">$<?php echo $priceList[0]; ?></span>
                            <span class="top">$<?php echo isset($_POST['price']) ? ($priceList[0] * $_POST['quantity'][0]) : "0"; ?></span>

                        </div>
                        <div class="productsList">
                            <!--<img src="product-images/camera.jpg"/><br/>-->
                            <span class="top">DIAMOND EARRINGS</span>
                            <span>Marquise diamonds bloom like exquisite flowers in this collection. In 18k rose gold with marquise diamonds. Size medium. Carat total weight 0.92. </span>
                            <span class="top">
                                <input type="number" min="0" name="quantity[]" value="<?php echo isset($_POST['quantity']) ? $_POST['quantity'][1] : "0"; ?>" />
                            </span>
                            <span class="top">$<?php echo $priceList[1]; ?></span>
                            <span class="top">$<?php echo isset($_POST['price']) ? ($priceList[1] * $_POST['quantity'][1]) : "0"; ?></span>

                        </div>
                        <div class="productsList">
                            <!--<img src="product-images/camera.jpg"/><br/>-->
                            <span class="top">THREE-ROW RING</span>
                            <span>Tiffany Jazz has rhythm. In platinum with round brilliant diamonds. Carat total weight 1.40. </span>
                            <span class="top">
                                <input type="number" min="0" name="quantity[]" value="<?php echo isset($_POST['quantity']) ? $_POST['quantity'][2] : "0"; ?>" />
                            </span>
                            <span class="top">$<?php echo $priceList[2]; ?></span>
                            <span class="top">$<?php echo isset($_POST['price']) ? ($priceList[2] * $_POST['quantity'][2]) : "0"; ?></span>

                        </div>
                        <div class="productsList">
                            <!--<img src="product-images/camera.jpg"/><br/>-->
                            <span class="top">MALTESE CROSS CLIP</span>
                            <span>Inspired by the symbol of Amalfi, Italy’s oldest republic. Clip in 18k gold with round brilliant diamonds. Carat total weight 1.18. </span>
                            <span class="top">
                                <input type="number" min="0" name="quantity[]" value="<?php echo isset($_POST['quantity']) ? $_POST['quantity'][3] : "0"; ?>" />
                            </span>
                            <span class="top">$<?php echo $priceList[3]; ?></span>
                            <span class="top">$<?php echo isset($_POST['price']) ? ($priceList[3] * $_POST['quantity'][3]) : "0"; ?></span>

                        </div>
                        <div class="productsList">
                            <!--<img src="product-images/camera.jpg"/><br/>-->
                            <span class="top">CHRONOGRAPH 42 MM</span>
                            <span>Men's chronograph watch in stainless steel. Blue soleil dial features silver poudré numerals. On a blue alligator strap. 42 mm case. Self-winding mechanical movement. Power reserve 42 hours. Water resistant to 100 meters/330 feet/10 ATM. Swiss-made. </span>
                            <span class="top">
                                <input type="number" min="0" name="quantity[]" value="<?php echo isset($_POST['quantity']) ? $_POST['quantity'][4] : "0"; ?>" />
                            </span>
                            <span class="top">$<?php echo $priceList[4]; ?></span>
                            <span class="top">$<?php echo isset($_POST['price']) ? ($priceList[4] * $_POST['quantity'][4]) : "0"; ?></span>
                        </div>
                    </div>
                </div>
                <p class = "total-price">Total Price: $<?php echo totalPrice(); ?></p>
                <input type="submit" value="Calculate Price" name="price" class="button"/><br/>
                <input type="submit" value="Place Order" name="submit" class="button"/><br/><br/>
            </form>

            <?php

            function validation() {
                if (isset($_POST['submit'])) {
                    $submittedQuatity = $_POST['quantity'];
                    $hasItem = FALSE;
                    foreach ($submittedQuatity as $i => $quantity) {
                        if ($quantity != 0) {
                            $hasItem = TRUE;
                            break;
                        }
                    }
                    if (!$hasItem) {
                        echo "<span style='color:red;'>Error: You have to order at least 1 item.</span> <br/>";
                    }
                }
                return $hasItem;
            }

            function totalPrice() {
                global $priceList;
                $totalPrice = 0;
                if (isset($_POST['price']) || isset($_POST['submit'])) {
                    $quantityList = $_POST['quantity'];
                    for ($i = 0; $i < count($priceList); $i++) {
                        $totalPrice += $quantityList[$i] * $priceList[$i];
                    }
                }
                return $totalPrice;
            }

            if (isset($_POST['submit'])) {

                if (validation()) {
                    $date = date("Y-m-d H.i.s");
                    $fileName = "OnlineOrders/" . $date . ".txt";
                    $onlineOrders = fopen($fileName, "w+");

                    if (is_writable($fileName)) {
                        $orderInfo = "HINGED BANGLE\t\t" . $priceList[0] . " x " . $_POST['quantity'][0] . " = " . ($priceList[0] * $_POST['quantity'][0]) . "\n" .
                                "DIAMOND EARRINGS\t\t" . $priceList[1] . " x " . $_POST['quantity'][1] . " = " . ($priceList[1] * $_POST['quantity'][1]) . "\n" .
                                "THREE-ROW RING\t\t" . $priceList[2] . " x " . $_POST['quantity'][2] . " = " . ($priceList[2] * $_POST['quantity'][2]) . "\n" .
                                "MALTESE CROSS CLIP\t\t" . $priceList[3] . " x " . $_POST['quantity'][3] . " = " . ($priceList[3] * $_POST['quantity'][3]) . "\n" .
                                "CHRONOGRAPH 42 MM\t\t" . $priceList[4] . " x " . $_POST['quantity'][4] . " = " . ($priceList[4] * $_POST['quantity'][4]) . "\n" .
                                "Order Total: \t\t\t" . "$" . totalPrice() . "\n" .
                                "Ordered at $date";
                        if (fwrite($onlineOrders, $orderInfo)) {

                            echo "<pstyle='color:green;'>Thank you for ordering! Your order information has been saved in the linked <a href='$fileName' target='_blank'>file</a>.</p>\n";
                        } else {
                            echo "<p> Cannot add your order here.</p>\n";
                        }
                    } else {
                        echo "<p>Cannot write to the file.</p>\n";
                        fclose($onlineOrders);
                    }
                }
            }
            ?>

        </div>
    </body>
</html>

