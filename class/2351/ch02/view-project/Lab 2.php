<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Tax Calculation</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
.wrapper{
	margin: 0 auto;
	width:30%;
	background-color: #F9F8F8;
	border: 1px solid #000000;
	padding:5px 0px 20px 10px;
	font-size: 100%;
	font-family: georgia,"times new roman",times,serif;
	line-height: 1.4;
}
input{
	margin-bottom: 10px;
}

</style>
</head>
<body>
<div class = "wrapper">
<h3>Tax Calculation</h3>
<h6>Note: if you don't enter any numeric data type, it will show error message.</h6>
	<form action="" method="post">
		<label>Enter your purchase amount</label>
		<input type="text" name="price" />
		<input type="submit" value = "Submit" />
	</form>
<?php
//var_dump($_POST);
if(isset($_POST['price'])) {
	$price1 = $_POST['price'];
	if (!(is_numeric($price1))){
			echo "<span style='color:red;'>Error: You entered a string. Please enter an Integer</span>";
			exit();
		}
			echo "<span style='color:green;'>Your tax is " . calculateTax($price1);
}
function calculateTax($price){
	$taxRate = .08;
	$taxOfPurchase = $taxRate*$price;
	return $taxOfPurchase;
}
?>
</div>
</body>
</html>