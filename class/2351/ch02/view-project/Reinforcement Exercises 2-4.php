<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Gas Prices</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <style type="text/css">
            .wrapper{
                margin: 0 auto;
                width:30%;
                background-color: #F9F8F8;
                border: 1px solid #000000;
                padding:5px 0px 20px 10px;
                font-size: 100%;
                font-family: georgia,"times new roman",times,serif;
                line-height: 1.4;
            }
        </style>
    </head>
    <body>
        <div class = "wrapper">
            <?php
            $GasPrice = 2.57;
            if ($GasPrice >= 2 && $GasPrice <= 3) {
                echo "<p>Gas prices are between $2.00 and $3.00</p>";
            } else {
                echo "<p>Gas prices are not between $2.00 and $3.00</p>";
            }
            ?>
        </div>
    </body>
</html>