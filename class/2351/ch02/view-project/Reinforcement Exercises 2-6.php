<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Is Even</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <style type="text/css">
            .wrapper{
                margin: 0 auto;
                width:30%;
                background-color: #F9F8F8;
                border: 1px solid #000000;
                padding:5px 0px 20px 10px;
                font-size: 100%;
                font-family: arial, georgia,"times new roman",times,serif;
                line-height: 1.4;
            }
            input{
                margin-bottom: 10px;
            }

        </style>
    </head>
    <body>
        <div class = "wrapper">
            <?php
            for ($f = 0; $f <= 100; $f++) {
                $c = (($f-32)*(5/9));
                $x = round($c, 1);
                echo "$f &deg;F = $x &deg;C</br>";
            }
            ?>
        </div>
    </body>
</html>