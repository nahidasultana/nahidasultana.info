<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Is Even</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
.wrapper{
	margin: 0 auto;
	width:30%;
	background-color: #F9F8F8;
	border: 1px solid #000000;
	padding:5px 0px 20px 10px;
	font-size: 100%;
	font-family: georgia,"times new roman",times,serif;
	line-height: 1.4;
}
input{
	margin-bottom: 10px;
}

</style>
</head>
<body>
<div class = "wrapper">
<h3>Odd or Even number</h3>
<h6>Note: if you don't enter any numeric, it will show you error message.</h6>
	<form action="" method="post">
		<label>Enter a number</label>
		<input type="text" name="number" />
		<input type="submit" value = "Submit" />
	</form>
<?php 	
	if($_POST){		
		$number = $_POST['number']; 
		//check if the enter value is a number or string, throw an error
		if (!(is_numeric($number))){
			echo "<span style='color:red;'>Error: You entered a string. Please enter an Integer</span>";
			exit();
		}		
		$number = round($number);	
		//divide the entered number by 2 
		//if the reminder is 0 then the number is even otherwise the number is odd
		if(($number % 2) == 0){
			echo "<span style='color:green;'>You entered an Even number</span>";
		}else{
			echo "<span style='color:green;'>You entered an Odd number</span>";
		}
	}	
?>
</div>
</body>
</html>