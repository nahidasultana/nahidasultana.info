<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Single Family Home</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
p{
	margin: 0 auto;
	width:30%;
	background-color: #F9F8F8;
	border: 1px solid #000000;
	padding:10px 0px 10px 10px;
	font-size: 100%;
	font-family: georgia,"times new roman",times,serif;
	line-height: 1.4;
}
</style>
</head>
<body>
<p>
<?php
$DaysEnglish = array("Sunday", "Monday", "TuesDay", "Wednesday", "Thursday", "Friday", "Saturday");
$DaysFrench = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
echo "<strong>The days of the week in English are:</strong><br/>";
echo "$DaysEnglish[0]<br/>";
echo "$DaysEnglish[1]<br/>";
echo "$DaysEnglish[2]<br/>";
echo "$DaysEnglish[3]<br/>";
echo "$DaysEnglish[4]<br/>";
echo "$DaysEnglish[5]<br/>";
echo "$DaysEnglish[6]<br/><br/>";
echo "<strong>The days of the week in French are:</strong><br/>";
echo "$DaysFrench[0]<br/>";
echo "$DaysFrench[1]<br/>";
echo "$DaysFrench[2]<br/>";
echo "$DaysFrench[3]<br/>";
echo "$DaysFrench[4]<br/>";
echo "$DaysFrench[5]<br/>";
echo "$DaysFrench[6]<br/>";
?>
</p>
</body>
</html>