<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Rounded Values</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
p{
	margin: 0 auto;
	width:30%;
	background-color: #F9F8F8;
	border: 1px solid #000000;
	padding:10px 0px 10px 10px;
	font-size: 100%;
	font-family: georgia,"times new roman",times,serif;
	line-height: 1.4;
}
</style>
</head>
<body>
<p>
<?php
echo "<strong>Let us the whole number is 7.45</strong><br/>";
$x = round(7.45);
$y = ceil(7.45);
$z = floor(7.45);
echo " The round is $x <br/>";
echo " The ceil is $y <br/>";
echo " The floor is $z <br/>";
?>
</p>
</body>
</html>