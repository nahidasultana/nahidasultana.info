<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Single Family Home</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
p{
	margin: 0 auto;
	width:35%;
	background-color: #F9F8F8;
	border: 1px solid #000000;
	padding:10px 0px 10px 10px;
	font-size: 100%;
	font-family: georgia,"times new roman",times,serif;
	line-height: 1.4;
}
</style>
</head>
<body>
<?php
$SingleFamilyHome = 399500;
$SingleFamilyHome_Display = number_format($SingleFamilyHome, 2);
echo "<p>The current median price of a single family home in Pleasanton, CA is $$SingleFamilyHome_Display.</p>";
?>
</body>
</html>