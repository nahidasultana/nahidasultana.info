<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Paycheck data</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="main">
                <?php

                function displayError($fieldName, $errorMsg) {
                    global $errorCount;
                    echo "Error: \"$fieldName\" $errorMsg<br/>\n";
                    ++$errorCount;
                }

                function validatePrice($data, $fieldName) {
                    global $errorCount;
                    if (empty($data) && $data != "0") {
                        displayError($fieldName, "<span style='color:red;'>This field is required.<br/></span>");
                        $retval = "";
                    } else { // Only clean up the input if it isn'y empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        if (!(is_numeric($retval))) {
                            displayError($fieldName, "<span style='color:red;'>must be a numeric value.<br/></span>");
                        } else if ($retval == 0) {
                            displayError($fieldName, "<span style='color:red;'>must be greater than zero.<br/></span>");
                        }
                    }
                    return($retval);
                }

                $errorCount = 0;
                $hours = validatePrice($_POST['hoursWorked'], "Hours Worked");
                $payrate = validatePrice($_POST['hourlyWages'], "Hourly Wages");
                if ($errorCount > 0)
                    echo "Please use the \"Back\" button to re-enter the data.<br/>\n";
                else {
                    $gross = $hours * $payrate;
                    if ($hours > 40) {
                        $remainder = $hours - 40;
                        $overtimeRate = $remainder * ($payrate * 0.5);
                        $totalSalary = $gross + $overtimeRate;
                        echo "Your regular paycheck is $$gross.<br/>";
                        echo "Your overtime rate is $$overtimeRate.<br/>";
                        echo "Your total paycheck is $$totalSalary.<br/><br/>";
                        echo '<a href = "paycheck.html">Calcultate another paycheck</a>';
                    } else {
                        echo "Your total paycheck is $$gross.<br/><br/>";
                        echo '<a href = "paycheck.html">Calcultate another paycheck</a>';
                    }
                }
                ?>
            </div>
        </div>
    </body>
</html>

