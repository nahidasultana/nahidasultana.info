<?php

function displayError($fieldName, $errorMsg) {
    global $errorCount;
    echo "Error: \"$fieldName\" $errorMsg<br/>\n";
    ++$errorCount;
}

function validate($data, $fieldName) {
    global $errorCount;
    if (empty($data) && $data != "0") {
        displayError($fieldName, "<span style='color:red;'>This field is required.</span>");
        $retval = "";
    } else { // Only clean up the input if it isn'y empty.
        $retval = trim($data);
        $retval = stripslashes($retval);
        if (!(is_numeric($retval))) {
            displayError($fieldName, "<span style='color:red;'>must be a numeric value.</span>");
        } else if ($retval == 0) {
            displayError($fieldName, "<span style='color:red;'>must be greater than zero.</span>");
        }
    }
    return($retval);
}

$errorCount = 0;
$SpeedA = validate($_POST['speedA'], "Speed of Train A");
$SpeedB = validate($_POST['speedB'], "Speed of Train B");
$Distance = validate($_POST['distances'], "Distances between trains");
if ($errorCount > 0)
    echo "Please use the \"Back\" button to re-enter the data.<br/>\n";
else {
    $DistanceA = (($SpeedA / $SpeedB) * $Distance) / (1 + ($SpeedA / $SpeedB));
    $DistanceA = round($DistanceA, 2);
    $DistanceB = $Distance - $DistanceA;
    $TimeA = round(($DistanceA / $SpeedA),2);
    $TimeB = round(($DistanceB / $SpeedB),2);
    echo "Train A traveled $DistanceA miles in $TimeA hours at $SpeedA mph.<br/>";
    echo "Train B traveled $DistanceB miles in $TimeB hours at $SpeedB mph.<br/>";
    echo "The sum of the two distances is $Distance miles";
}