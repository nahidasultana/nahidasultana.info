<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Contact Me</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <div class="main">
                <?php
                function validateInput($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        echo "\"$fieldName\" <span style='color:red;'>This field is required.</span><br/>\n";
                        ++$errorCount;
                        $retval = "";
                    } else { // Only clean up the input if it isn't empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                    }
                    return ($retval);
                }

                function validateEmail($data, $fieldName) {
                    global $errorCount;
                    if (empty($data)) {
                        echo "\"$fieldName\" <span style='color:red;'>This field is required.</span><br/>\n";
                        ++$errorCount;
                        $retval = "";
                    } else { // Only clean up the input if it isn't empty.
                        $retval = trim($data);
                        $retval = stripslashes($retval);
                        $pattern = "/^[_a-z0-9-+]+(\.[_a-z0-9-+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
                        if (preg_match($pattern, $retval) == 0) {
                            echo "\"$fieldName\" <span style='color:red;'>is not a valid e-mail address.</span><br/>\n";
                            ++$errorCount;
                        }
                    }
                    return ($retval);
                }

                $ShowForm = TRUE;
                $errorCount = 0;
                $Sender = "";
                $Email = "";
                $Subject = "";
                $Message = "";
                if (isset($_POST['Submit'])) {
                    $Sender = validateInput($_POST['Sender'], "Your Name");
                    $Email = validateEmail($_POST['Email'], "Your E-mail");
                    $Subject = validateInput($_POST['Subject'], "Subject");
                    $Message = validateInput($_POST['Message'], "Message");
                    if ($errorCount == 0) {
                        $ShowForm = FALSE;
                    } else {
                        $ShowForm = TRUE;
                    }
                }
                if ($ShowForm == TRUE) {
                    if ($errorCount > 0) //if there were errors
                        echo "<p>Please use the \"Back\" button to re-enter the form information.</p>\n";
                } else {
                    $SenderAddress = "$Sender <$Email>";
                    $Headers = "From: $SenderAddress\nCC:$SenderAddress\n";
                    // Substitue your own email address for recipient@example.com
                    $result = mail("nahidasultana@outlook.com", $Subject, $Message, $Headers);
                    if ($result)
                        echo "<p style = 'color:green'>Your message has been sent. Thank you," . $Sender . ".</p>\n";
                    else {
                        echo "<p style = 'color:red'>There was an error sending your message," . $Sender . ".</p>\n";
                    }
                }
                ?>
            </div>
        </div>
    </body>
</html>